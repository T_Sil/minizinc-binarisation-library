#!/bin/bash
echo "Flattening model with 2 passes..."

# If no arguments given
if [ $# -eq 0 ]; then
    echo "ERROR: Missing parameters"
    echo "Usage: mzn_encode {-l [encoding library file]} [model].mzn"
    exit
fi

# Set initial value of encoding library
encode_lib=""

# Iterate through command line args
while getopts "l:h" OPTION
do
	case $OPTION in
		l)
            # If a library will be used in the second pass of the encoding
			encode_lib=$OPTARG
			;;
		h)
			echo "Usage: mzn_encode {-l [encoding library file]} [model].mzn"
			;;
	esac
done

# Extract the last command line argument (should be the model)
mzn_model=${@: -1}
# Check if the model is a .mzn file 
if [[ ${@: -1} =~ \.mzn$ ]]; then
    mzn_model=${@: -1}
else 
    echo
    echo "ERROR: Missing miniZinc model"
    echo "Usage: mzn_encode {-l [encoding library file]} [model].mzn"
    exit
fi

# Extract model name without extension
model_name="${mzn_model%.*}"

# First flattening step (using linear library)
minizinc -c -Glinear -o t3mp_fzn_0ut.fzn $mzn_model

# Change "output_var" annotation to "original_output_var"
sed -i -e 's/output_var/original_output_var/g' t3mp_fzn_0ut.fzn

# Introduce the new annotation in the model
sed -i "1iannotation original_output_var;" t3mp_fzn_0ut.fzn

# Rename generated minizinc object file
mv "$model_name.ozn" pass1.ozn

# If an encoding library was given, append it to the output of the first pass
if [ "$encode_lib" != "" ]; then
    # Append an include for the encoding library to the top of the fzn file
    sed -i "1iinclude \"$encode_lib\";" t3mp_fzn_0ut.fzn
fi

# Change flatzinc extension to minizinc extension
mv t3mp_fzn_0ut.fzn t3mp_fzn_0ut.mzn

# Rerun mzn2fzn on new file
minizinc -c -o "$model_name.fzn" t3mp_fzn_0ut.mzn

# Rename new generated minizinc object file
mv t3mp_fzn_0ut.ozn pass2.ozn

cp pass2.ozn convert_solution.mzn

# Remove intermediate minizinc file
rm t3mp_fzn_0ut.mzn

echo "Model flattened"
exit 0